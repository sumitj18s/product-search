import { Action, Reducer } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootAction, RootState } from ".";
import config from "../config";

const getInventoryStructure = (state: InventoryState) =>
  state.allIds.map((id) => state.byId[id]);

export interface Inventory {
  id: string;
  createdTime: string;
  fields: {
    Posted: string;
    "Product Code": string;
  };
}

export interface InventoryState {
  fetching: boolean;
  sending: boolean;
  byId: { [id: string]: Inventory };
  allIds: string[];
  filterRecords: string[];
  queryString: string;
}

const initialState = {
  fetching: false,
  sending: false,
  byId: {},
  allIds: [],
  filterRecords: [],
  queryString: "",
};

export const inventoryReducer: Reducer<InventoryState, RootAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case FETCH_INVENTORY:
      return {
        ...state,
        fetching: true,
      };
    case FETCH_INVENTORY_SUCCESS:
      return {
        ...state,
        fetching: false,
        byId: action.payload.reduce((byId, item) => {
          byId[item.id] = item;
          return byId;
        }, {}),
        allIds: action.payload.map((item) => item.id),
      };
    case FETCH_INVENTORY_ERROR:
      return {
        ...state,
        fetching: false,
      };
    case SEND_INVENTORY:
      return {
        ...state,
        sending: true,
      };
    case SEND_INVENTORY_SUCCESS:
    case SEND_INVENTORY_ERROR:
      return {
        ...state,
        sending: false,
      };
    case FILTER_RECORDS: {
      const filterProduct = getInventoryStructure(state).filter((item) =>
        item.fields[action.searchField].includes(action.searchFieldValue)
      );

      return {
        ...state,
        filterRecords: filterProduct,
        queryString: action.searchFieldValue,
      };
    }
    default:
      return state;
  }
};

const FETCH_INVENTORY = "FETCH_INVENTORY";
const FETCH_INVENTORY_SUCCESS = "FETCH_INVENTORY_SUCCESS";
const FETCH_INVENTORY_ERROR = "FETCH_INVENTORY_ERROR";
const SEND_INVENTORY = "SEND_INVENTORY";
const SEND_INVENTORY_SUCCESS = "SEND_INVENTORY_SUCCESS";
const SEND_INVENTORY_ERROR = "SEND_INVENTORY_ERROR";
const FILTER_RECORDS = "FILTER_RECORDS";

interface FilterRecordAction extends Action<typeof FILTER_RECORDS> {
  searchField: string;
  searchFieldValue: string;
}

interface FetchInventoryAction extends Action<typeof FETCH_INVENTORY> {}
interface FetchInventorySuccessAction
  extends Action<typeof FETCH_INVENTORY_SUCCESS> {
  payload: Inventory[];
}
interface FetchInventoryErrorAction
  extends Action<typeof FETCH_INVENTORY_ERROR> {
  error: boolean;
  payload: Error;
}
interface SendInventoryAction extends Action<typeof SEND_INVENTORY> {}
interface SendInventorySuccessAction
  extends Action<typeof SEND_INVENTORY_SUCCESS> {
  payload: Inventory;
}
interface SendInventoryErrorAction extends Action<typeof SEND_INVENTORY_ERROR> {
  error: boolean;
  payload: Error;
}
export type InventoryAction =
  | FetchInventoryAction
  | FetchInventorySuccessAction
  | FetchInventoryErrorAction
  | SendInventoryAction
  | SendInventorySuccessAction
  | SendInventoryErrorAction
  | FilterRecordAction;

export const actions = {
  searchRecords: (filter: {
    searchField: string;
    searchFieldValue: string;
  }): ThunkAction<void, RootState, undefined, InventoryAction> => (
    dispatch
  ) => {
    dispatch({ type: FILTER_RECORDS, ...filter });
  },
  fetchInventory: (): ThunkAction<
    void,
    RootState,
    undefined,
    InventoryAction
  > => (dispatch) => {
    dispatch({ type: FETCH_INVENTORY });
    fetch(
      "https://api.airtable.com/v0/appJkRh9E7qNlXOav/Home?offset=0&maxRecords=100&view=Grid%20view",
      {
        headers: {
          Authorization: config.Authorization,
        },
      }
    )
      .then((response) => response.json())
      .then((body) => {
        dispatch({
          type: FETCH_INVENTORY_SUCCESS,
          payload: body.records,
        });
      })
      .catch((e) => {
        dispatch({
          type: FETCH_INVENTORY_ERROR,
          error: true,
          payload: e,
        });
      });
  },

  sendInventory: (
    data: string,
    goBack: () => void
  ): ThunkAction<void, RootState, undefined, InventoryAction> => (
    dispatch,
    getState
  ) => {
    if (getState().inventory.sending) {
      return;
    }
    dispatch({ type: SEND_INVENTORY });
    fetch(
      "https://api.airtable.com/v0/appJkRh9E7qNlXOav/Home?maxRecords=100&view=Grid%20view",
      {
        method: "POST",
        headers: {
          Authorization: config.Authorization,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          fields: {
            "Product Code": data,
          },
        }),
      }
    )
      .then((response) => response.json())
      .then((body) => {
        dispatch({ type: SEND_INVENTORY_SUCCESS, payload: body });
        goBack();
      })
      .catch((e) => {
        dispatch({
          type: SEND_INVENTORY_ERROR,
          error: true,
          payload: e,
        });
      });
  },
};

export const selectors = {
  selectInventory: (state: RootState) =>
    state.inventory.queryString
      ? state.inventory.filterRecords
      : getInventoryStructure(state.inventory),
};
