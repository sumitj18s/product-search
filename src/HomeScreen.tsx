import { MaterialCommunityIcons } from "@expo/vector-icons";
import React, { useRef } from "react";
import {
  RefreshControl,
  ScrollView,
  StyleSheet,
  View,
  Text,
  TextInput,
  Button,
} from "react-native";
import { Appbar, DataTable, FAB } from "react-native-paper";
import {
  NavigationEvents,
  SafeAreaView,
  NavigationInjectedProps,
} from "react-navigation";
import { useSelector, useDispatch } from "react-redux";
import { selectors, actions } from "./store/inventory";
import { RootState } from "./store";

export default (props: NavigationInjectedProps) => {
  const fetching = useSelector((state: RootState) => state.inventory.fetching);
  const inventory = useSelector(selectors.selectInventory);
  const handlerInput = useRef(null);
  const dispatch = useDispatch();

  const setFilter = (value: string) =>
    dispatch(
      actions.searchRecords({
        searchField: "Product Code",
        searchFieldValue: value,
      })
    );

  const resetFilter = (value: string) => {
    setFilter(value);
    handlerInput.current.clear();
  };

  return (
    <View style={{ flex: 1 }}>
      <NavigationEvents
        onWillFocus={() => dispatch(actions.fetchInventory())}
      />

      <Appbar.Header>
        <Appbar.Content title="Inventory" />
      </Appbar.Header>

      <View style={styles.searchSection}>
        <TextInput
          style={styles.searchField}
          ref={handlerInput}
          onChangeText={setFilter}
          placeholder="Please enter a product code to filter:"
        />
        <Button title="Reset" onPress={() => resetFilter("")}></Button>
      </View>
      <View>
        <Text style={styles.textProductCount}>
          Total # of Product codes found are: {inventory.length}
        </Text>
      </View>
      <ScrollView
        style={{ flex: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={fetching}
            onRefresh={() => dispatch(actions.fetchInventory())}
          />
        }
      >
        <SafeAreaView>
          <DataTable>
            <DataTable.Header>
              <DataTable.Title>Product Code</DataTable.Title>
              <DataTable.Title numeric>Scan Date</DataTable.Title>
            </DataTable.Header>
            {inventory.map((record, index) => (
              <DataTable.Row key={index}>
                <DataTable.Cell>{record.fields["Product Code"]}</DataTable.Cell>
                <DataTable.Cell numeric>
                  {new Date(record.fields.Posted).toLocaleDateString()}{" "}
                  {new Date(record.fields.Posted).toLocaleTimeString()}
                </DataTable.Cell>
              </DataTable.Row>
            ))}
          </DataTable>
        </SafeAreaView>
      </ScrollView>

      <SafeAreaView style={styles.fab}>
        <FAB
          icon={() => (
            <MaterialCommunityIcons name="barcode" size={24} color="#0B5549" />
          )}
          label="Scan Product"
          onPress={() => props.navigation.navigate("Camera")}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    flex: 1,
    alignItems: "center",
  },
  searchSection: {
    padding: 20,
    flexDirection: "row",
  },
  textProductCount: { paddingLeft: 20, color: "#0B5549" },
  searchField: {
    borderColor: "gray",
    width: 300,
    height: 50,
    borderWidth: 1,
    marginRight: 10,
  },
});
